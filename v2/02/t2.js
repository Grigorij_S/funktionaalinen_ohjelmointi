'use strict';

const Auto = (function(){
    
    const suojatut = new WeakMap();
    
    class Auto{
        konstruktori(){
            suojatut.set(this, {tankki: 0, matkamittari: 0});
        }
        
        aja() {
            let mittari = this.getMatkamittari();
            mittari += 1;
            let polttoaine = this.getTankki();
            polttoaine -= 1;
            suojatut.set(this, {tankki: polttoaine, matkamittari: mittari});
        }
        
        tankkaa(määrä) {
            suojatut.set(this, {tankki: this.getTankki() + määrä, matkamittari: this.getMatkamittari()});
        }
        
        getMatkamittari() {
            return suojatut.get(this).matkamittari;
        }
        
        getTankki() {
            return suojatut.get(this).tankki;
        }
    }
    
    return Auto;
})();


const auto = new Auto();
console.log("Matkamittari: " + auto.getMatkamittari() + "Tankki " + auto.getTankki());
auto.tankkaa(50);
auto.aja();
console.log("Matkamittari: " + auto.getMatkamittari() + "Tankki " + auto.getTankki());
auto.aja();
console.log("Matkamittari: " + auto.getMatkamittari() + "Tankki " + auto.getTankki());
