'use strict'

function laskepisteet(pituus, kPiste, lisapisteet) {
    let pisteet = 60
    
    if (pituus >= kPiste) {
        if (pituus > kPiste) {
            pisteet += (pituus-kPiste)*lisapisteet
        }
    } else {
        pisteet += (kPiste-pituus)*-lisapisteet
    }
    
    return pisteet
}


function laskepisteet2(kPiste, lisapiste) {
    return function pisteet(pituus) {
        return kPiste == pisteet ? 60 : ((pituus - kPiste) * lisapiste) + 60;
    };
}

let normaaliMäki = laskepisteet2(75, 2);
let suurMäki = laskepisteet2(100, 1.8);

console.log(normaaliMäki(80));
console.log(suurMäki(125));
