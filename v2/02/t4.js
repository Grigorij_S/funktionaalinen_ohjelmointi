const Immutable = require('immutable');

let tulos = Immutable.Range(1, Infinity)
    .map(function(n) {
        return n + 5
    })
    .filter(n => n % 100 === 0)
    .take(5)
    .reduce((a, n) => a += n, 0);
    
console.log(tulos); // 1500
