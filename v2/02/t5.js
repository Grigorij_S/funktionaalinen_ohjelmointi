const Immutable = require('immutable');

var firstTenElements = Immutable.Repeat()
                                .map((x=10) => x*10)
                                .take( 10 )
                                .toJSON();

console.log(firstTenElements);
