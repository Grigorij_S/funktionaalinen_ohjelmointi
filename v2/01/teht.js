/*Kirjoita uudestaan käyttäen nuolifunktioita (arrow functions).

function toCelsius(fahrenheit) {
    return (5/9) * (fahrenheit-32);
}

function area(radius)   
    {  
        return Math.PI * radius * radius;  
};*/
   
var fs = require('fs') 
    

// TEHT. 1
var toCelsius = (fahrenheit) => (5/9) * (fahrenheit-32);
var area = (radius) => Math.PI * radius * radius;


// TEHT. 2
var lista
fs.readFile('movies.js','utf-8', function(err, data) {
    if (err) throw err
    lista = JSON.parse(data)
    
    let uusiLista = lista.map((x) => (
        {title:x.title, release:x.release}))
        
    // TEHT. 3
    // console.log(lista.filter(x => x.release > 2011))
})

// TEHT. 4
let lt2015 = [-7, -4, 0, 2, 4, 8, 10, 13, 5, 0, -3, -9]
let lt2016 = [-5, -3, 1, 3, 2, 6, 15, 9, 3, 2, -1, -7]


console.log(   lt2015
                .map((x, i) => (x + lt2016[i])/2)
                .filter(x => x > 0)
                .reduce((prev, cur, index, array) => {
                    prev += cur
                    if (index == array.length-1) {
                      prev/= array.length
                    }
                    //console.log(prev)
                    return prev
                },0))
                
// TEHT. 5
fs.readFile('kalevala.txt', 'utf-8', function(err, data) {
    
    if (err) throw err
    
    let lista = []
    let lista2 = []
    lista = data.match(/[^,.:;! \r\n][\wåäöÅÄÖ']*/g)

    lista.map(x => {
        if (lista2.findIndex(i => i.sana === x) < 0) {
            lista2.push(
                {sana: x, määrä:
                    lista.reduce((acc, current) => {
                        if (current === x) {
                            acc++
                        }
                        return acc
                    },0)
                }
            )
        }
    })

    lista2.sort((a, b) => a.sana.localeCompare(b.sana)).map(x => {console.log(x)})
})