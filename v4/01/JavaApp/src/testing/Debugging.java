package testing;

import static org.junit.Assert.*;

import java.util.*;
import java.util.stream.Collectors;

import org.junit.Test;

public class Debugging{
    public static void main(String[] args) {

    	testi();
    }


    private static class Point{
        private int x;
        private int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }
        
        public int getY() {
			return y;
		}

        public void setX(int x) {
            this.x = x;
        }
        
        
		public void setY(int y) {
			this.y = y;
		}
        
		
        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }
        
        
        @Override
        public boolean equals(Object obj) {
        	if(obj != null && obj instanceof Point){
        		Point p = (Point)obj;
        		if(p.x == this.x && p.y == this.y)
        			return true;
        	}
        	return false;
        }
        
        
        public static List<Point> moveAllPointsRightBy(List<Point> points, int i) {
    		return points.stream().map(p -> p.moveRightBy(i))
    				.collect(Collectors.toList());
    	}
    }
    @Test
	public static void testi() {
		List<Point> points = Arrays.asList(new Point(5,5), new Point(10,5));
		List<Point> expectedPoints = Arrays.asList(new Point(15,5), new Point(20,5));
		List<Point> movedPoints = Point.moveAllPointsRightBy(points, 10);
		points.stream().forEach(e -> System.out.println("Original: "+e.getX() + " " + e.getY()));
		System.out.println("");
		expectedPoints.stream().forEach(e -> System.out.println("Expected: "+e.getX() + " " + e.getY()));
		System.out.println("");
		movedPoints.stream().forEach(e -> System.out.println("Moved: "+e.getX() + " " + e.getY()));
		
		assertEquals(expectedPoints, movedPoints);
	}
}
