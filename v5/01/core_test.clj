(ns lein-midje-proj.core-test
    (:use lein-midje-proj.core)
    (:use  midje.sweet))

(facts "square tehtävä 4"
  (square 2) => 4
  (square 7) => 49
  (square -3) => 9)

(tabular "karkausvuosi? tehtävä 5 "
  (fact
    (karkausvuosi? ?vuosi) => ?expected)
    ?vuosi   ?expected
    100     false
    200     false
    400     true
    800     true
    2000    true
    2200    false
    12      true
    20      true
    15      false
    1913    false)
  
  
  
  (+ 4 (* 2 5))

(+ 1 2 3 4 5)

((fn [name] (str "Tervetuloa Tylypahkaan " name)) "Grigorj")


(def person {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}})
(get-in person [:name :middle])


(defn 
  square
  ([x] (* x x))
)
(square 7)


(defn
  karkausvuosi?
    [v]
    (if (>= v 100) 
      (if (= (rem v 400) 0) true false)
      (if (= (rem v 4) 0) true false))
)
(karkausvuosi? 100)
(karkausvuosi? 200)
(karkausvuosi? 400)
(karkausvuosi? 800)
(karkausvuosi? 2000)
(karkausvuosi? 2200)
(karkausvuosi? 12)
(karkausvuosi? 20)
(karkausvuosi? 15)
(karkausvuosi? 1913)