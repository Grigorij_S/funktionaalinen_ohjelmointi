'use strict'

const f = function () {
    return function (x) {
		return x+1;
	}
}(); // Huomaa funktion kutsu!

let tulos = f(3);

console.log(tulos);


// TEHT 1
const vertaa = function() {
	return function(eka, toka) {
		if(eka > toka) {
			return 1
		} else if (eka < toka) {
			return -1
		} else {
			return 0
		}
	}
}();

console.log("TEHT 1")
console.log("vertaa(0,1) --> "+vertaa(0,1))
console.log("vertaa(1,1) --> "+vertaa(1,0))
console.log("vertaa(1,1) --> "+vertaa(1,1))


//TEHT 2
const vertaa2 = function(vertaa, t1, t2) {
	let tulos = 0;
	
	for(let i=0; i<t1.length; i++) {
		if (vertaa(t1[i], t2[i]) == -1){
			tulos++
		}
	}
	return tulos
}

console.log("TEHT 2")
console.log("vertaa2(vertaa, [22, 33], [23, 34]) 2 --> "+vertaa2(vertaa, [22, 33], [23, 34]))
console.log("vertaa2(vertaa, [22, 33], [20, 34]) 1 --> "+vertaa2(vertaa, [22, 33], [20, 34]))
console.log("vertaa2(vertaa, [22, 33], [20, 30]) 0 --> "+vertaa2(vertaa, [22, 33], [20, 30]))