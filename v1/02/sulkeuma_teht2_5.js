'use strict';

let f, g;
function foo() {
  let x;
  f = function() { return ++x; };
  g = function() { return --x; };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  
console.log('call to g(): ' + g()); 
console.log('call to f(): ' + f()); 



/*

Kun foo() kutsutaan luodaan x,

kun foo() on suoritettu x häviää, mutta f ja g silti viittaavat x:ään, koska molemmat ovat sulkeumassa saman x:n kanssa

       _>  x  <_
      /        \ 
      |        |
     f()       g()


Se vaan toimii

  ¯\_(ツ)_/¯

*/