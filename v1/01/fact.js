function fact(n) {
// triviaalitapaus
	if (n === 0) {
		return 1;
	}
 // perussilmukka
	return n * fact(n - 1);
}
var tulos = fact(4);
console.log(tulos);

// TEHT 1
function onPalindromi(merkkijono) {
	if(merkkijono.length < 2) {
		return true;
	} else if (merkkijono.charAt(0) != merkkijono.charAt(merkkijono.length-1)) {
		return false;
	} else {
		//return onPalindromi(merkkijono.substr(1).slice(0, -1))
		return onPalindromi(merkkijono.substr(1, merkkijono.length-2))
	}
}

console.log("")
console.log("TEHT 1")
console.log("sokos: "+onPalindromi("sokos"))
console.log("imaami: "+onPalindromi("imaami"))
console.log("hello: "+onPalindromi("hello"))


// TEHT 2
function syt(p, q) {
	if (q == 0) {
		return p;
	} else {
		return syt(q, p%q)
	}
}

console.log("")
console.log("TEHT 2")
console.log("102 ja 68 = 34 --> "+syt(102, 68))
console.log("102 ja 0 = 102 --> "+syt(102, 0))

//TEHT 3
//jaottomia jos p:n ja q:n suurin yhteinen tekijä on 1
function kjl(p, q) {
	if (q == 1) {
		return true;
	}
	return syt(p,q) == 1
}

console.log("")
console.log("TEHT 3")
console.log("35 ja 18 = jaottomia true --> "+kjl(35, 18))
console.log("102 ja 68 = jaollisia (34) false --> "+kjl(102, 68))

//TEHT 4 
//Kirjoita potenssiin korotus rekursiivisena funktiona. 
function potenssi(num, exp) {
	if (exp == 0) {
		return 1
	} else {
		return num * potenssi(num, exp-1)
	}
}

console.log("")
console.log("TEHT 4")
console.log("2^4 = 16 --> "+potenssi(2, 4))
console.log("100^0 = 1 --> "+potenssi(100, 0))

// v1 02
// HÄNTÄREKURSSIIVINEN
function potenssi2(num, pot) {
	return helpPotenssi(num, pot, 1)
}

function helpPotenssi(num, pot, acc) {
	if (pot == 0) {
		return acc
	} else {
		return helpPotenssi(num, pot-1, acc*num)
	}
}

console.log("")
console.log("v1 02, TEHT 4")
console.log("2^4 = 16 --> "+potenssi2(2, 4))
console.log("100^0 = 1 --> "+potenssi2(100, 0))


//TEHT 5
//[0,1,2,3,4,5,6,7,8,9] on käännettynä [ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]

function kaanna(lista, alku, loppu) {
	if (alku === loppu || alku - loppu === 1) {
		return lista;
	} else {
		let apu = lista[alku]
		lista[alku] = lista[loppu]
		lista[loppu] = apu
		
		return kaanna(lista, alku+1, loppu-1)
	}
}

console.log("")
console.log("TEHT 5")
console.log("[0,1,2,3,4,5,6,7,8,9] --> "+kaanna([0,1,2,3,4,5,6,7,8,9,], 0, 9))
console.log("[0,1,2,3,4,5,6,7,8,9,10] --> "+kaanna([0,1,2,3,4,5,6,7,8,9,10], 0, 10))