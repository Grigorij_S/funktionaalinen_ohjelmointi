import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.IntStream;


//function laskepisteet2(kPiste, lisapiste) {
//    return function pisteet(pituus) {
//        return kPiste == pisteet ? 60 : ((pituus - kPiste) * lisapiste) + 60;
//    };
//}


// 		http://www.geeksforgeeks.org/anonymous-inner-class-java/
//interface Arvonta {
//	ArrayList<Integer> lottorivi = new ArrayList<Integer>();
//	
//	public default ArrayList<Integer> Arvaa() {
//		ThreadLocalRandom.current().ints(0, 39).distinct().limit(7).forEach((k)->lottorivi.add(k));
//		return lottorivi;
//	}
//}


interface Fibonacci {
//	IntSupplier edellinen = 0;
	int nykyinen = 0;
	
	IntSupplier f();
}



//https://www.tutorialspoint.com/java/java_innerclasses.htm
class Outer {
   // private variable of the outer class
   private ArrayList<Integer> lottorivi = new ArrayList<Integer>(); 
   
   // inner class
   public class Inner {
      public ArrayList<Integer> getNums() {
    	  ThreadLocalRandom.current().ints(0, 39).distinct().limit(7).forEach((k)->lottorivi.add(k));
//    	  IntStream.generate(()->{return (int)(Math.random()*39);}).limit(10).forEach((k)->lottorivi.add(k));
    	  return lottorivi;
      }
   }
}


class Fib_Outer {
   // private variable of the outer class
   private int edellinen, nykyinen, edellistäEdellinen; 
   
   // inner class
   public class Fib_Inner {
      public int fibonacci() {    	  
    	  
    	  if (edellinen == 0) {
    		  edellinen = 1;
    		  nykyinen = 1;
    		  edellistäEdellinen = 0;
    	  } else {
    		  edellistäEdellinen = edellinen;
    		  edellinen = nykyinen;
    		  nykyinen = edellistäEdellinen + edellinen;
    	  }
    	  
          return edellistäEdellinen;
      }
   }
}



public class Makihyppy {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet){
            return (pituus) -> (pituus - kPiste) * lisapisteet + 60;
    }
        
    public static void main(String[] args) {
    	
    	
    	//TEHT. 1
    	IntSupplier is = ()-> (int)(Math.random() * 6 + 1);
        System.out.println("TEHT. 1: "+is.getAsInt());

        
        //TEHT. 2
        DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 1.8);
        System.out.println("TEHT. 2: "+normaaliLahti.applyAsDouble(100)); 
        
        
        //TEHT. 3
        System.out.print("TEHT. 3, TAPA 1: ");
        ThreadLocalRandom.current().ints(0, 39).distinct().limit(7).forEach((k)->System.out.print(k + ", "));
//        IntStream.generate(()->{return (int)(Math.random()*39);}).limit(10).forEach((k)->System.out.print(k + ", "));
        
        System.out.println("");
        System.out.print("TEHT. 3, TAPA 2: ");
        
//        Arvonta arv = new Arvonta() {};
//        System.out.println(arv.Arvaa());
        
        // Instantiating the outer class
        Outer outer = new Outer();
        
        // Instantiating the inner class
        Outer.Inner inner = outer.new Inner();
        System.out.println(inner.getNums());
        
        
        //TEHT. 4 Vinkki: käytä instanssimuuttujia edellinen ja nykyinen. Paluuarvona on edellistä edellinen.
        System.out.print("TEHT. 4: ");
        Fib_Outer fib_outer = new Fib_Outer();
        Fib_Outer.Fib_Inner fib_inner = fib_outer.new Fib_Inner();
        
        IntStream.generate(()->{return fib_inner.fibonacci();}).limit(10).forEach((k)->System.out.print(k + ", "));       
//        IntStream.generate(fibo).limit(10).forEach((n) -> System.out.println(n));
        
        
        //TEHT. 5
        System.out.print("TEHT. 5: ");
        
          
        
    }
    
}