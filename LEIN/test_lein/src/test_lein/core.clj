(ns test-lein.core
  (:gen-class))

(def person {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}})

(defn 
    square
    ([x] (* x x))
)

(defn
    karkausvuosi?
      [v]
      (if (>= v 100) 
        (if (= (rem v 400) 0) true false)
        (if (= (rem v 4) 0) true false))
)

(defn -main
  "I do a whole lot ... now."
  [& args]
  (println "Hello, World!")
  
  (println "")
  (println "TEHT. 3")
  (println(+ 4 (* 2 5)))
  (println(+ 1 2 3 4 5))
  (println((fn [name] (str "Tervetuloa Tylypahkaan " name)) "Grigorj"))
  (println(get-in person [:name :middle]))
  (println "")
  (println "TEHT. 4")
  (println(square 7))
  (println "")
  (println "TEHT. 5")
  (println(karkausvuosi? 100))
  (println(karkausvuosi? 200))
  (println(karkausvuosi? 400))
  (println(karkausvuosi? 800))
  (println(karkausvuosi? 2000))
  (println(karkausvuosi? 2200))
  (println(karkausvuosi? 12))
  (println(karkausvuosi? 20))
  (println(karkausvuosi? 15))
  (println(karkausvuosi? 1913))
  
)