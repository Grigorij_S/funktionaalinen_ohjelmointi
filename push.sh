if (( $# < 1 )); then
    echo "Must enter a message"
    exit
fi

git add .
git commit -m $1
git push origin master